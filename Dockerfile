FROM node:14.15.4-alpine3.10
WORKDIR /app
COPY pakage*.json ./
RUN npm install
COPY . ./
EXPOSE 3000
CMD ["node", "src/index.js"]