const axios = require('axios')
const Service = require('./services.model')
const { readFileSync } = require('fs')
const path = require('path')

exports.getAuth = async (headers) => {
    const authURI = await getServiceUri('auth')
    let res = await axios({
        method: 'get',
        url: `${authURI}/auth/info`,
        headers: { ssid: headers.ssid }
    })
    if (res.data.success)
        return res.data.data
    else
        return null
}

const getServiceUri = async (key) => {
    const service = await Service.findOne({ key }, { uri: 1, _id: 0 })
    if (!service || !service.uri)
        return null
    return service.uri
}

exports.getServiceUri = getServiceUri

exports.seedServices = async () => {
    if (process.env.SERVICES_FILE && path.extname(path.resolve(process.env.SERVICES_FILE)) == '.json') {
        const data = JSON.parse(readFileSync(path.resolve(process.env.SERVICES_FILE),{encoding: 'utf-8'}))
        if (data && data.services) {
            console.log("seeding services from file")
            for (const service of data.services) {
                console.log(`saving service: ${service.key}`)
                let vali = await Service.findOne({$or: [{key: service.key}, {uri: service.uri}]})
                if (vali && vali._id) {
                    console.log('Service key or uri is already registered, skipping...')
                    continue;
                }
                let result = await storeService(service)
                if (result.success)
                    console.log(`service ${service.key} saved!`)
                else {
                    console.log('one service cannot be saved')
                    console.table(result.data)
                    console.log('Cleaning database...')
                    await Service.collection.drop()
                    console.log('Database is empty')
                }
            }
        }
    }
}

const storeService = async (base) => {
    try {
        let data = new Service(base)
        var error = data.validateSync();
        if(error)
        {
            let status = []
            for(const key in error.errors)
            {
                let message = error.errors[key].message
                let res = {
                    field: error.errors[key].path,
                    reason: message.toString().replace(/\\|"/gi,"")
                };
                status.push(res);
            }
            return { success: false, info: 'Invalid data structure', data:status}
        } else{
            let status = await data.save();
            if(!('_id' in status)) {
                return { success: false, info: 'Fatal Error, unable to store service, try later'}
            }else {
                return { success: true, info: 'Service save successfully', data: status}
            }
        }
    } catch (error) {
        next(error)
    }
}

exports.storeService = storeService

exports.health = async (req, res, next) => {
    try {
        let services = await Service.find()
        if (services == null || services == undefined)
            return res.status(503).send('FAIL')

        return res.status(200).send('OK')
    } catch (error) {        
            return res.status(503).send('FAIL')
    }
}

exports.AdminMiddleware = async (req, res, next) => {
    try {
        if (!req.headers["admin"] || !req.headers["ssid"] || req.headers["ssid"] !== process.env.ADMIN_PASSWORD) {
            return res.status(403).send('You dont have access to this, fuck off')
        }
        next()
    } catch (error) {
        next(error)
    }
}