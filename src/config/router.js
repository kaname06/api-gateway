const axios = require('axios');
const { Router } = require('express')
const { getAuth, getServiceUri } = require('./functions')
const router = Router()

router.all('/:service/*', async (req, res, next) => {
    let user = null;
    if (req.headers['ssid']) {
        user = await getAuth(req.headers)
    }
    // return res.send('ok')
    service = await getServiceUri(req.params.service)
    if (!service)
        return res.status(400).json({success: false, info: 'Unable to reach this service'})
    axios({
        method: req.method,
        url: `${service}/${req.params[0]}`,
        headers: user == null ? req.headers : {...req.headers, userId: user._id, userRole: user.role},
        data: req.body
    }).then(response => {
        res.set({...response.headers})
        res.status(response.status).json(response.data)
    })
    .catch(err => next(err.message))
})

module.exports = router