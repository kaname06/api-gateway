const driver = require('mongoose')
const { seedServices } = require('./functions')

driver.set('debug', true)
try {
    driver.connect(`mongodb://${process.env.MDB_USER}:${process.env.MDB_PASSWORD}@${process.env.MDB_HOST}:${process.env.MDB_PORT}/${process.env.MDB_DB_NAME}`, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true
    })
    .then(db => {
        console.log("database connected")
        seedServices()
    })
    .catch(err => console.log(err))
} catch (error) {
    console.error(error)
    process.exit(1)
}