const { Schema, model } = require('mongoose')

const Service = new Schema({
    key: {
        type: String,
        required: true,
        unique: true,
        index: true
    },
    uri: {
        type: String,
        required: true
    },
    prefix: {
        type: String,
        default: null
    },
    port: {
        type: Number,
        default: null
    },
    host: {
        type: String,
        default: null
    }
})

module.exports = model('Service', Service)