const { Router } = require('express')
const router = Router()
const controller = require('./controller')
const { AdminMiddleware } = require('../config/functions')

router
    .route('/service')
    .get(AdminMiddleware, controller.getAllService)
    .post(AdminMiddleware, controller.storeService)

module.exports = router