const Service = require('../config/services.model')
const { storeService } = require('../config/functions')

exports.getAllService = async (req, res, next) => {
    try {
        const data = await Service.find()
        return res.status(200).json({success: true, info: 'Query do it successfully', data: data})
    } catch (error) {
        next(error)
    }
}

exports.storeService = async (req, res, next) => {
    const response = await storeService(req.body)
    if (response && response.success) {
        return res.status(200).json(response)
    }
    else {
        return res.status(400).json(response)
    }
}