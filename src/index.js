const express = require('express')
const { health } = require('./config/functions')
require('./config/database')
const app = express()
const PORT = 3000
app.use(express.json())

app.get('/health', health)

app.use('/admin', require('./admin/router'))
app.use('/:service', require('./config/router'))


app.listen(PORT, () => console.log(`Server running on port: ${PORT}`))